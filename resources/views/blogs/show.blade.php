@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2> Show Blogs</h2>
</div>
<div class="pull-right">
<a class="btn btn-primary" href="{{ route('adminblogs.index') }}"> Back</a>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Title:</strong>
{{ $blog->title }}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Content:</strong>
{{ $blog->content }}
</div>
</div>
</div>
</div>
</div>
@endsection