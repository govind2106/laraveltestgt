<?php
namespace App\Http\Controllers; 
use App\Blog;
use Illuminate\Http\Request;
class blogController extends Controller
{


 public function __construct()
    {
        $this->middleware('auth');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$blogs = Blog::orderby('id', 'desc')->get();
return view('blogs.index',compact('blogs'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
return view('blogs.create');
}
/**
* Store a newly created resource in storage.
*
* @param \Illuminate\Http\Request $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$request->validate([
'title' => 'required',
'content' => 'required',
]);
Blog::create($request->all());
return redirect()->route('adminblogs.index')
->with('success','blogs created successfully.');
}
/**
* Display the specified resource.
*
* @param \App\Blog $blog
* @return \Illuminate\Http\Response
*/
public function show(Request $request)
{

	$blog = Blog::where('id', $request->segment(2))->first();
	
return view('blogs.show',compact('blog'));
}
/**
* Show the form for editing the specified resource.
*
* @param \App\Blog $blog
* @return \Illuminate\Http\Response
*/
public function edit(Request $request)
{
	$blog = Blog::where('id', $request->segment(2))->first();
return view('blogs.edit',compact('blog'));
}
/**
* Update the specified resource in storage.
*
* @param \Illuminate\Http\Request $request
* @param \App\Blog $blog
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Blog $blog)
{
$request->validate([
'title' => 'required',
'content' => 'required',
]);

$blog = Blog::where('id', $request->segment(2))->update(array('title' => $request->title,'content'=>$request->content));
return redirect()->route('adminblogs.index')
->with('success','blogs updated successfully');
}
/**
* Remove the specified resource from storage.
*
* @param \App\Blog $blog
* @return \Illuminate\Http\Response
*/
public function destroy(Request $request)
{
	$blog = Blog::where('id', $request->segment(2))->delete();

return redirect()->route('adminblogs.index')
->with('success','Blog deleted successfully');
}
}