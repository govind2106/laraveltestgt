<?php

namespace App\Http\Middleware;

use Closure;
use App\Blog;


class DeleteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        Blog::truncate();
        return $next($request);
    }
}
