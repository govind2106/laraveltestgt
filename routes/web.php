<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','HomeController');

Route::resource('/blogs','HomeController');
Auth::routes();

Route::Group([ 'middleware' => ['auth']], function () {
Route::resource('adminblogs','blogController');
Route::resource('adminblogs','blogController');
});

Route::Group(['middleware' => ['auth','deleteall']], function () {

	Route::resource('deleteall','blogController');

});



