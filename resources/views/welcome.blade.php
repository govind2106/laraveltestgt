@extends('layouts.front')
@section('content')
  <div class="row justify-content-center">
    <div class="col-md-10">

@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
<tr>
<th>Title</th>
<th>Content</th>
<th width="280px">Action</th>
</tr>
@foreach ($blogs as $blog)
<tr>
<td>{{ $blog->title }}</td>
<td>{{ $blog->content }}</td>
<td>
<form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">
<a class="btn btn-info" href="{{ route('blogs.show',$blog->id) }}">Show</a>

</form>
</td>
</tr>
@endforeach
</table>
</div>
</div>
@endsection